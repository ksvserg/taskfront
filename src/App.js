import React, { Component } from 'react';
import './App.css';
import List from './components/list';
import axios from 'axios';

const OFFSET = 0;
const LIMIT = Math.round((document.documentElement.clientHeight) / 40);
const ORDER = '-created';
const API_SERVER = 'http://127.0.0.1:3001';

class App extends Component {
  constructor(props) {
    super(props);
    this.fetchItems = this.fetchItems.bind(this);
    this.onAdd = this.onAdd.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onChangeOrder = this.onChangeOrder.bind(this);
    this.onReset = this.onReset.bind(this);
    this.onSearch = this.onSearch.bind(this);
    this.onRemove = this.onRemove.bind(this);
    this.onScroll = this.onScroll.bind(this);
    this.state = {
      list: [],
      name: '',
      description: '',
      priority: '',
      offset: OFFSET,
      limit: LIMIT,
      order: ORDER,
      hasMore: true,
      loading: false,
    }
  }

  componentDidMount() {
    this.resetList();
    document.addEventListener('scroll', this.onScroll);
  }

  /*
    Refresh data from server
  */
  fetchItems(order, offset, name, description, priority) {
    this.setState({ loading: true });
    axios.get(`${API_SERVER}/list?order=${order}&offset=${offset}&limit=${LIMIT}&name=${name}&description=${description}&priority=${priority}`).then(
        (response) => {
          const { list } = this.state;
          this.setState({
            list: (offset > 0) ? list.concat(response.data.list) : response.data.list,
            hasMore: response.data.hasMore,
            loading: false,
          })
        }
    )
  }

  /*
   Add new task
   */
  onAdd() {
    const { name, description, priority } = this.state;
    if (!name || !description || !priority) alert('Enter all fields!');

    axios.post(`${API_SERVER}/tasks`, {
      name,
      description,
      priority,
    }).then(
        (response) => {
          this.setState({
            name: '',
            priority: '',
            description: '',
          });
          this.resetList();
        }
    )
  }

  /*
   Change filter value
   */
  onChange(key, value) {
    const state = {};
    state[key] = value;
    this.setState(state);
  }

  /*
   Change table order
   */
  onChangeOrder(field) {
    const { order, name, description, priority } = this.state;
    let newOrder = field;
    if (order.indexOf(field) === 0) {
      newOrder = `-${field}`;
    }
    window.scrollTo(0,0);
    this.setState({
      order: newOrder,
      limit: LIMIT,
      offset: OFFSET,
    });
    this.fetchItems(newOrder, OFFSET, name, description, priority);
  }

  onRemove(id) {
    axios.post(`${API_SERVER}/remove`, {
      id
    }).then(
        (response) => {
          this.resetList();
        }
    )
    return false;
  }


  /*
   Reset current filter and order to defaults
   */
  onReset() {
    this.setState({
      name: '',
      priority: '',
      description: '',
      order: ORDER,
      offset: OFFSET,
      limit: LIMIT,
    });
    this.resetList();
  }


  /*
   Search tasks by fields
   */
  onSearch() {
    const { name, description, priority } = this.state;
    window.scrollTo(0,0);
    this.setState({
      order: ORDER,
      offset: OFFSET,
      limit: LIMIT,
    });
    this.fetchItems(ORDER, OFFSET, name, description, priority);
  }

  /*
   Load new data if we end of page
   */
  onScroll(event) {
    const { name, description, priority } = this.state;
    if (document.body.scrollHeight ===
        document.body.scrollTop +
        window.innerHeight) {
      const { order, offset, limit, hasMore } = this.state;
      if (!hasMore) return;

      const newOffset = offset + limit;
      this.fetchItems(order, newOffset, name, description, priority);
      this.setState({ offset: newOffset });
    }
  }

  resetList() {
    window.scrollTo(0,0);
    this.fetchItems(ORDER, OFFSET, '', '', '');
  }

  render() {
    const { list, order, name, description, priority } = this.state;
    return (
        <div className="app">
          <nav className="navbar navbar-inverse navbar-fixed-top">
            <a href="#">Task queue</a>
          </nav>
          <section className="content-area">
            <div className="table-area">
              <List
                data={list}
                onAdd={this.onAdd}
                onChange={this.onChange}
                onChangeOrder={this.onChangeOrder}
                onReset={this.onReset}
                onRemove={this.onRemove}
                onSearch={this.onSearch}
                order={order}
                name={name}
                priority={priority}
                description={description}
              />
            </div>
          </section>
        </div>
    );
  }
}

export default App;
