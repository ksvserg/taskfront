import React, { Component, PropTypes } from 'react';
import Arrow from './arrow';
import '../App.css';

class List extends Component {
    constructor(props) {
        super(props);
        this.onNameChange = this.onNameChange.bind(this);
        this.onDescriptionChange = this.onDescriptionChange.bind(this);
        this.onPriorityChange = this.onPriorityChange.bind(this);
        this.renderRow = this.renderRow.bind(this);
    }

    onNameChange(e) {
        this.props.onChange('name', e.target.value);
    }

    onDescriptionChange(e) {
        this.props.onChange('description', e.target.value);
    }

    onPriorityChange(e) {
        this.props.onChange('priority', e.target.value);
    }

    renderRow(task) {
        const { name, description, priority, _id } = task;
        return (
            <tr key={_id["$oid"]}>
                <td>{name}</td>
                <td>{description}</td>
                <td>{priority}</td>
                <td>
                    <a onClick={() => this.props.onRemove(_id["$oid"])} href="#" >
                        <span className="glyphicon glyphicon-remove" ></span>
                    </a>
                </td>
            </tr>
        )
    }

    render() {
        const { data, order, onChangeOrder, name, priority, description } = this.props;
        return (
            <table className="responsive-table table">
                <thead>
                    <tr>
                        <th onClick={() => onChangeOrder('name')}>
                            <input 
                                placeholder="Name" 
                                className="form-control" 
                                onChange={this.onNameChange}
                                onClick={(e) => e.stopPropagation()}
                                value={name}
                            />
                            <br/>
                            Name
                            {(order === 'name') && <Arrow up />}
                            {(order === '-name') && <Arrow />}
                        </th>
                        <th onClick={() => onChangeOrder('description')}>
                            <input 
                                placeholder="Description" 
                                className="form-control" 
                                onChange={this.onDescriptionChange}
                                onClick={(e) => e.stopPropagation()}
                                value={description}
                            />
                            <br/>
                            Description
                            {(order === 'description') && <Arrow up />}
                            {(order === '-description') && <Arrow />}
                        </th>
                        <th onClick={() => onChangeOrder('priority')}>
                            <input 
                                placeholder="Priority" 
                                className="form-control" 
                                type="number"
                                onClick={(e) => e.stopPropagation()}
                                onChange={this.onPriorityChange}
                                value={priority}
                            />
                            <br/>
                            Priority
                            {(order === 'priority') && <Arrow up />}
                            {(order === '-priority') && <Arrow />}
                        </th>
                        <th>
                            <div className="btn-group" role="group" >
                                <button type="button" className="btn btn-default" onClick={this.props.onAdd} >Add</button>
                                <button type="button" className="btn btn-default" onClick={this.props.onSearch} >Search</button>
                                <button type="button" className="btn btn-default" onClick={this.props.onReset}>Reset</button>
                            </div>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {data.map(this.renderRow)}
                </tbody>
            </table>
        )
    }
}

List.propTypes = {
    data: PropTypes.array,
    onAdd: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    onChangeOrder: PropTypes.func.isRequired,
    onReset: PropTypes.func.isRequired,
    onRemove: PropTypes.func.isRequired,
    onSearch: PropTypes.func.isRequired,
    order: PropTypes.string,
    name: PropTypes.string,
    priority: PropTypes.string,
    description: PropTypes.string,
}

export default List;