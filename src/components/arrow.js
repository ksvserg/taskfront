import React from 'react';

function Arrow(props) {
    if (props.up)
        return (
            <span className="dropup">
                <span className="caret"></span>
            </span>
        );
    return (
        <span className="caret"></span>
    )
}

export default Arrow;