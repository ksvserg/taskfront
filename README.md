# Frontend part of Task queue #

This is a part of task queue application.

![Screenshot from 2017-04-03 01-17-05.png](https://bitbucket.org/repo/oLqX4qA/images/4063062332-Screenshot%20from%202017-04-03%2001-17-05.png)

### Installation ###

* npm i
* npm start
* By default API server is running on http://127.0.0.1:3001 If you need to change it, edit src/App.js